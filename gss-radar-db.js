const { getGeoTiffPaths, getWindow, getRainRate } = require('./gss-rain-radar'),
    axios = require('axios'),
    yauzl = require('yauzl'),
    GeoTiff = require('geotiff'),
    { DateTime } = require('luxon'),
    sqlite3 = require('sqlite3'),
    Logger = require('./logger'),
    logger = Logger.getLogger('module:gss-radar-db', false),
    db = new sqlite3.Database('rain-radar.db'),
    //db = new sqlite3.Database('/Users/allan/Desktop/sensors.db'),
    // Östra Sömmerskestigen 5: N 6389808, E 697437
    // Visby flygplats: N 6396165, E 699618
    // Visby D: N 6392144, E 696557
    // Mästerby: N 6374715, E 696310
    NORTH_POINT = 6389808,
    EAST_POINT = 697437,
    START_TIME = '2008-10-01T00:00:00.000Z'; //first date when SMHI has published rain radar data
/**
 * Creates the rain radar database tables
 * @returns {Promise}
 */
function createDataBaseRadarTable() {
    return new Promise((resolve, reject) => {
        const createRadarRainTable =
                'create TABLE if not EXISTS RadarRainData (datetime string PRIMARY KEY, rain real);',
            createUpdateTimeTable =
                'create TABLE if not EXISTS RadarRainDataLastUpdate (datetime string PRIMARY KEY);';
        db.exec(createRadarRainTable + createUpdateTimeTable, err => {
            if (err) {
                logger.error(
                    'Error in creating tables for radar rain data',
                    err
                );
                return reject(err);
            } else {
                return resolve();
            }
        });
    });
}
/**
 * Inits the radar last updated rain database table
 * @returns {Promise}
 */
function initRadarRainLastUpdate() {
    return new Promise((resolve, reject) => {
        const insertStartUpdateTime =
            `INSERT INTO RadarRainDataLastUpdate VALUES ("${START_TIME}")`;
        db.exec(insertStartUpdateTime, err => {
            if (err) {
                logger.error(
                    'Error in init of last update table for radar rain data',
                    err
                );
                return reject(err);
            } else {
                return resolve('2008-10-01T00:00:00.000Z');
            }
        });
    });
}

/**
 * Clear the radar rain data base table
 * @returns {Promise}
 */
// eslint-disable-next-line no-unused-vars
function clearRadarTable() {
    return new Promise((resolve, reject) => {
        db.exec(
            'DROP TABLE IF EXISTS RadarRainData;DROP TABLE IF EXISTS RadarRainDataLastUpdate;',
            err => {
                if (err) {
                    logger.error('Error in dropping RadarRainData table', err);
                    reject(err);
                } else {
                    return resolve();
                }
            }
        );
    });
}
/**
 * Prepare insert rain value into radar table statement
 * @returns {Promise}
 */
function prepareInsertRadarRain() {
    return new Promise((resolve, reject) => {
        let statement = db.prepare(
            'INSERT INTO RadarRainData VALUES (?,?)',
            err => {
                if (err) {
                    logger.error('Error in creating prepared statement', err);
                    reject(err);
                } else {
                    resolve(statement);
                }
            }
        );
    });
}
/**
 * Insert a post in the database
 * @param {String} dateTimeString
 * @param {float} rainValue
 * @param {Statement} dbStatement
 * @returns {Promise}
 */
function insertRainData(dateTimeString, rainValue, dbStatement) {
    return new Promise((resolve, reject) => {
        dbStatement.run(dateTimeString, rainValue, err => {
            if (err) {
                logger.error('Error in running statement', err);
                reject(err);
            } else {
                resolve();
            }
        });
    });
}
/**
 * Insert the date when rain data was last processed
 * @param {String} dateTimeIsoString - ISO date time string
 * @returns {Promise}
 */
function updateLastUpdateTime(dateTimeIsoString) {
    return new Promise((resolve, reject) => {
        db.run(
            'UPDATE RadarRainDataLastUpdate SET DateTime = ?  WHERE rowid = ?',
            dateTimeIsoString,
            1,
            err => {
                if (err) {
                    logger.error('Error updating last update timestamp', err);
                    reject(err);
                } else {
                    resolve(err);
                }
            }
        );
    });
}
/**
 * Get last update time from database
 * @returns {Promise<String>} - Resolves to an ISO date time string
 */
function getLastUpdateTime() {
    return new Promise((resolve, reject) => {
        db.get(
            'SELECT DateTime FROM RadarRainDataLastUpdate WHERE rowid = ?',
            1,
            async (err, data) => {
                if (err) {
                    return reject(err);
                } else {
                    if (!data) {
                        let datetime = await initRadarRainLastUpdate();
                        return resolve(datetime);
                    } else {
                        let { datetime } = data;
                        return resolve(datetime);
                    }
                }
            }
        );
    });
}
/**
 * Fetch zip-file from smhi with radar data
 * @param {String} downloadLink - url
 * @returns {Promise<Buffer>}
 */
function fetchFile(downloadLink) {
    return new Promise((resolve, reject) => {
        let data = [];
        axios({
            method: 'get',
            url: downloadLink,
            responseType: 'stream'
        })
            .then(resp => {
                resp.data.on('data', chunk => {
                    data.push(chunk);
                });
                resp.data.on('end', () => {
                    return resolve(Buffer.concat(data));
                });
                resp.data.on('error', err => {
                    logger.error('Error when downloading file', err);
                    return reject(err);
                });
            })
            .catch(err => {
                logger.error('Error when starting to download file', err);
                return reject(err);
            });
    });
}
/**
 * Convert file name to ISO date time format
 * @param {String} fileName
 * @returns {String} - ISO date time string
 */
function getValidTimeStampForEntry(fileName) {
    let [, date_part] = fileName.split('_'),
        [date] = date_part.split('.');
    return (
        '20' +
        date[0] + // Year
        date[1] +
        '-' +
        date[2] + // Month
        date[3] +
        '-' +
        date[4] + // Day
        date[5] +
        'T' +
        date[6] + // Hour
        date[7] +
        ':' +
        date[8] + // Minutes
        date[9] +
        ':00.000Z'
    );
}
/**
 * Unzip file buffer
 * @param {Buffer} zipBuffer - Array buffer
 * @param {[function]} bufferProcessor - Optional
 * @returns {Promise<Array>} - Promise which resolves to an Array
 */
function unZip(zipBuffer, bufferProcessor) {
    return new Promise((resolve, reject) => {
        let allData = [];
        yauzl.fromBuffer(
            zipBuffer,
            { lazyEntries: true, decodeStrings: false },
            (err, zipFile) => {
                if (err) {
                    logger.error('Error opening zip-buffer.', err);
                    reject(err);
                } else {
                    zipFile.readEntry();
                    zipFile.on('entry', entry => {
                        entry.fileName = entry.fileName.toString('utf8');
                        if (/\/$/.test(entry.fileName)) {
                            // Directory file names end with '/'.
                            // Note that entires for directories themselves are optional.
                            // An entry's fileName implicitly requires its parent directories to exist.
                            zipFile.readEntry();
                        } else if (/^\/radar*/.test(entry.fileName)) {
                            logger.debug('Unzipping entry: ' + entry.fileName);
                            let validTimestamp = getValidTimeStampForEntry(
                                entry.fileName
                            );
                            zipFile.openReadStream(entry, (err, readStream) => {
                                let data = [];
                                if (err) {
                                    logger.err('Error open entry', err);
                                    zipFile.readEntry();
                                } else {
                                    readStream.on('data', chunk => {
                                        data.push(chunk);
                                    });
                                    readStream.on('error', err => {
                                        logger.error(
                                            'Error processing entry:',
                                            err
                                        );
                                        zipFile.readEntry();
                                    });
                                    readStream.on('end', () => {
                                        if (bufferProcessor) {
                                            bufferProcessor(Buffer.concat(data))
                                                .then(processedData => {
                                                    allData.push({
                                                        dateTime: validTimestamp,
                                                        rainValue: processedData
                                                    });
                                                    zipFile.readEntry();
                                                })
                                                .catch(err => {
                                                    logger.error(
                                                        'Error processing data',
                                                        err
                                                    );
                                                    reject(err);
                                                });
                                        } else {
                                            allData.push({
                                                dateTime: validTimestamp,
                                                data: data
                                            });
                                            zipFile.readEntry();
                                        }
                                    });
                                }
                            });
                        } else {
                            zipFile.readEntry();
                        }
                    });
                    zipFile.on('end', () => {
                        resolve(allData);
                    });
                }
            }
        );
    });
}
/**
 * Class for extracting rain data from an geo tiff image for a specific point
 */
class RainRadar {
    constructor(northPoint, eastPoint) {
        this.northPoint = northPoint;
        this.eastPoint = eastPoint;
        this.timeIntervall = 5;
    }

    /**
     * Extract rain data from a geo tiff image
     * @param {Buffer} buffer
     * @returns {Promise<Number>}
     */
    async getRainFromRadarImage(buffer) {
        let geotiff = await GeoTiff.fromArrayBuffer(
            buffer.buffer.slice(
                buffer.byteOffset,
                buffer.byteOffset + buffer.byteLength
            )
        );
        this.image = await geotiff.getImage();
        let raster = await this.image.readRasters({
                window: this.window
            }),
            radarData = (getRainRate(raster[0]) * this.timeIntervall) / 60;
        return radarData;
    }

    get window() {
        if (!this.calculatedWindow) {
            this.calculatedWindow = getWindow(
                this.northPoint,
                this.eastPoint,
                this.image,
                0
            );
        }
        return this.calculatedWindow;
    }
}
function isToday(date) {
    return date.hasSame(DateTime.local(), 'day');
}

/**
 * Fetches radar data for the date
 * @param {DateTime} date - Date when radar data should be fetch and processed for
 * @param {DateTime} lastUpdateTime - When the data was last updated
 * @returns {Promise<Array>} - Array of radar data
 */
async function getRainDataForDate(date, lastUpdateTime) {
    let currentPoint = new RainRadar(NORTH_POINT, EAST_POINT);
    try {
        let { data } = await getGeoTiffPaths(date),
            { downloads, files } = data,
            totalData = [];
        if (isToday(date)) {
            let filesToProcess = files.filter(({ key }) => {
                let timeStamp = getValidTimeStampForEntry(key);
                return timeStamp > lastUpdateTime;
            });
            for (let index = 0; index < filesToProcess.length; index++) {
                const { key, formats } = filesToProcess[index],
                    [format] = formats,
                    { link } = format,
                    fileBuffer = await fetchFile(link),
                    rainValue = await currentPoint.getRainFromRadarImage(
                        fileBuffer
                    );
                totalData.push({
                    dateTime: getValidTimeStampForEntry(key),
                    rainValue: rainValue
                });
            }
        } else {
            let [download] = downloads,
                { link } = download,
                zipFile = await fetchFile(link);
            totalData = await unZip(zipFile, async buffer => {
                return await currentPoint.getRainFromRadarImage(buffer);
            });
        }
        return totalData;
    } catch (error) {
        logger.error('Error processing date: ' + date.toISO(), error);
        throw error;
    }
}
/**
 * Set up database and statement
 * @returns {Promise<Statement>}
 */
async function setUp() {
    try {
        await createDataBaseRadarTable();
    } catch (err) {
        logger.error('Error in setup function', err);
    }
    return await prepareInsertRadarRain();
}
/**
 * Main function, executing with 5 min intervall
 * @param {Statement} statement - A prepared statement for inserting radar rain data
 */
async function main(statement) {
    const lastUpdateTime = await getLastUpdateTime();
    let dateToProcess = DateTime.fromISO(lastUpdateTime);
    let endDate = DateTime.local();
    while (dateToProcess.startOf('day') < endDate.endOf('day')) {
        try {
            let result = await getRainDataForDate(
                    dateToProcess,
                    lastUpdateTime
                ),
                totalRain = 0,
                updateTime;
            for (let index = 0; index < result.length; index++) {
                let { dateTime, rainValue } = result[index];
                updateTime = dateTime;
                if (rainValue > 0) {
                    try {
                        await insertRainData(dateTime, rainValue, statement);
                        totalRain += rainValue;
                    } catch (err) {
                        if (err.errno === 19) {
                            logger.info(
                                `Got unique constraint for ${dateTime}`
                            );
                        } else {
                            throw err;
                        }
                    }
                }
                await updateLastUpdateTime(updateTime);
            }
            logger.info(
                dateToProcess.toISODate() +
                    ' ' +
                    ~~(totalRain * 10) / 10 +
                    ' mm.'
            );
        } catch (err) {
            logger.error(
                `Error in main function for date: ${dateToProcess.toISODate()}`,
                err
            );
        }
        dateToProcess = dateToProcess.plus({ day: 1 });
    }
    logger.info('Main function done for now');
    setTimeout(() => {
        main(statement);
    }, 5 * 60 * 1000);
}
async function run(logLevel = 'info') {
    Logger.setLogLevel(logLevel);
    try {
        const statement = await setUp();
        main(statement);
    } catch (error) {
        logger.error('Error starting radar data collection', error);
        throw error;
    }
}
run();
