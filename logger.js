let loggers = {},
    logLevel = 'debug',
    logLevels = {
        debug: 1,
        info: 2,
        warn: 3,
        error: 4
    };
function logMessage(level, logTimestamp, name, msg, obj) {
    let time = new Date(),
        objMsg,
        log = console;
    if (obj) {
        try {
            objMsg = `"${JSON.stringify(obj)}"`;
        } catch (error) {
            objMsg = '';
        }
    }
    log[level](
        `${
            logTimestamp ? time.toISOString() : ''
        } ${level.toUpperCase()} ${name} ${msg}${objMsg ? ' ' + objMsg : ''}`
    );
    if (obj && obj.stack) {
        log['trace'](obj.stack);
    }
}
class Logger {
    constructor(name, logTimestamp = false) {
        this.name = name;
        this.logTimestamp = logTimestamp;
        loggers[name] = this;
    }
    _shouldLog(myLogLevel) {
        let currentLogLevel = logLevels[logLevel],
            selfLogLevel = logLevels[myLogLevel];
        return selfLogLevel >= currentLogLevel;
    }
    debug(msg, obj) {
        this._shouldLog('debug') &&
            logMessage('debug', this.logTimestamp, this.name, msg, obj);
    }
    info(msg, obj) {
        this._shouldLog('info') &&
            logMessage('info', this.logTimestamp, this.name, msg, obj);
    }
    warn(msg, obj) {
        this._shouldLog('warn') &&
            logMessage('warn', this.logTimestamp, this.name, msg, obj);
    }
    error(msg, obj) {
        this._shouldLog('error') &&
            logMessage('error', this.logTimestamp, this.name, msg, obj);
    }
}
function setLogLevel(newLogLevel) {
    if (logLevels[newLogLevel]) {
        logLevel = newLogLevel;
    }
}
function getLogger(name, logTimestamp = false) {
    if (!loggers[name]) {
        loggers[name] = new Logger(name, logTimestamp);
    }
    return loggers[name];
}
module.exports = {
    getLogger,
    setLogLevel
};
