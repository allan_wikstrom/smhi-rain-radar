/* See https://opendata.smhi.se/apidocs/radar/data.html */
const axios = require('axios'),
    { DateTime } = require('luxon'),
    logger = require('./logger').getLogger('module:gss-rain-radar');
/**
 *
 * @param {Number} northPoint
 * @param {Number} eastPoint
 * @param {GeoTIFF} geoTiffImage
 * @param {Number} size
 * @returns {Array}
 */
function getWindow(northPoint, eastPoint, geoTiffImage, size = 0) {
    let [minX, , , maxY] = geoTiffImage.getBoundingBox(),
        [resolutionX, resolutionY] = geoTiffImage.getResolution(),
        northPixel = (northPoint - maxY) / resolutionY,
        eastPixel = (eastPoint - minX) / resolutionX,
        left = Math.floor(eastPixel) - size,
        top = Math.floor(northPixel) - size,
        right = Math.ceil(eastPixel) + size,
        bottom = Math.ceil(northPixel) + size,
        precision = Math.sqrt(
            (0.5 - (eastPixel - left)) ** 2 + (0.5 - (northPixel - bottom)) ** 2
        );

    return [left, top, right, bottom, precision];
}
/**
 * Convert pixel value to dBZ value
 * @param {Number} pixelValue
 * @returns {Number}
 */
function pixel2dBZ(pixelValue) {
    return pixelValue * 0.4 - 30;
}
/**
 * Convert dBZ value to rain rate
 * @param {Number} dBZValue
 * @returns {Number}
 */
function dBZ2RainRate(dBZValue) {
    if (dBZValue < 18) {
        return 0;
    }
    if (dBZValue > 70) {
        logger.warn('Abnormal dbZValue: ' + dBZValue);
    }
    return Math.pow(10, Math.log10(5 * Math.pow(10, 0.1 * dBZValue - 3)) / 1.5);
}
/**
 * Converts a radar array with pixel values to rain rate (mm/h)
 * @param {Array<Number>} radarArray
 * @param {function} statFunc - Aggregate function
 * @returns {Number}
 */
function getRainRate(radarArray, statFunc = Math.max) {
    let resultArray = [];
    radarArray.forEach(pixelValue => {
        if (pixelValue === 0 || pixelValue === 255) {
            resultArray.push(0);
        } else {
            resultArray.push(dBZ2RainRate(pixel2dBZ(pixelValue)));
        }
    });
    return statFunc(...resultArray);
}
/**
 * Fetch urls to smhi geo tiffs
 * @param {Object} date
 * @returns {Object}
 */
async function getGeoTiffPaths(date = DateTime.local()) {
    return await axios.get(
        `https://opendata-download-radar.smhi.se/api/version/latest/area/sweden/product/comp/${
            date.year
        }/${date.toFormat('LL')}/${date.toFormat('dd')}?format=tif`
    );
}

module.exports = {
    getRainRate,
    getWindow,
    getGeoTiffPaths
};
