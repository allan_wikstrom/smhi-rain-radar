# README #

This script fetches geo tiffs from SMHI and extract and stores the rain rate into a SQLite data base for a specific coordinate.

## Installation ##

* Install latest LTS version of node.js [nodejs.org](https://nodejs.org/en/download/)
* Install dependencies `npm install`

## Usage ##

* Specify the coordinates where the rain rate should be calculated for. Use SWEREF99 coordinate system. The coordinates are specified in `gss-radar-db.js` as the following constants `NORTH_POINT` and `EAST_POINT`.
* As default the script will fetch and calculate rain history as well, from SMHI, since 2008-10-01. If this is not necessary, it is possible too specify another start time in `gss-radar-db.js` with the constant `START_TIME`.
* Start the script `npm run`. It will then start to fetch rain data from SMHI from `START_TIME` to now and then check for new rain data every 5 minutes

## Browse the data ##

The best way to browse the data is with [DB Browser for SQLite](https://sqlitebrowser.org/)

There are two tables `RadarRainData` and `RadarRainDataLastUpdate`

In `RadarRainData` the data is stored. It contains two columns, date time and rain. Where rain is how many millimeters rain it has detected the last 5 minutes, i.e unit is mm

## Further information ##

[Radar data from SMHI](https://opendata.smhi.se/apidocs/radar/data.html)